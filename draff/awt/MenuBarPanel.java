package draff.awt;

import draff.awt.event.*;
import java.awt.*;

/**
 * A class to display the options of a menu bar on a panel.
 * Currently it just displays the menu items as buttons.
 */
public class MenuBarPanel extends Container
{	
	public MenuBarPanel (MenuBar whatToShow) {
		setLayout(new FlowLayout());
		
		MenuItem currentItem;		
		for (int menuIndex = 0; menuIndex < whatToShow.getMenuCount(); menuIndex++)
		{
			for (int menuItemIndex = 0; menuItemIndex < whatToShow.getMenu(menuIndex).getItemCount();
					menuItemIndex++) {				
				currentItem = whatToShow.getMenu(menuIndex).getItem(menuItemIndex);
				
				// A "-" for the menu item's label indicates a separator
				if (!currentItem.getLabel().equals("-")) {
					Button displayButton = new Button(currentItem.getLabel());				
					displayButton.addActionListener(new HandOverEventActionListener(currentItem));
					add(displayButton);
				}
			}
		}
	}
}
