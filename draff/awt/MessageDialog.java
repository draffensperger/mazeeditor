package draff.awt;
import draff.awt.event.*;
import java.awt.*;

public class MessageDialog extends Dialog {	
	public MessageDialog(Frame parent, String title, String text) {
		super (parent);
		
		addWindowListener(new WindowCloseWindowListener());
		
		if (text.indexOf("\n") != -1) {
			((TextArea)add(new TextArea(text))).setEditable(false);
		} else {
			add(new Label(text));
		}
		setTitle(title);
		setVisible(true);
		pack();
	}	
}
