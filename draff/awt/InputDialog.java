package draff.awt;

import draff.awt.event.*;
import java.awt.*;
import java.util.Vector;
import java.awt.event.*;

public class InputDialog extends Dialog {
	public Vector textFields;
		
	public InputDialog(Frame parent, String title, String[] inputNames, String[] defaultValues) {
		super(parent);
		init(title, inputNames, defaultValues);
	}
	
	public InputDialog(Frame parent, String title, String inputName, String defaultValue) {
		super(parent);
		String[] inputNames = new String[1];
		inputNames[0] = inputName;
		
		String[] defaultValues = new String[1];
		defaultValues[0] = defaultValue;
		
		init(title, inputNames, defaultValues);
	}
	
	private void init(String title, String[] inputNames, String[] defaultValues) {
		addWindowListener(new WindowCloseWindowListener());
		setTitle(title);
		
		/*int numRowBreaks = 0;
		
		for (int inputNameIndex = 0; inputNameIndex < inputNames.lengh; inputNameIndex++) {
			if (inputNames[inputNameIndex].startsWith("/n") {
				inputNames[inputNameIndex] = inputNames[inputNameIndex].substring(1, inputNames[inputNameIndex].length());
				numRowBreaks++;
			}
		}
		
		setLayout(new GridLayout(1 + numRowBreaks,1));*/		
		
		setLayout(new FlowLayout());
		setModal(true);
		
		textFields = new Vector();
		
		for (int textFieldIndex = 0; textFieldIndex < inputNames.length; textFieldIndex++) {
			TextField currentField;
			
			if (textFieldIndex < defaultValues.length) {
				currentField = new TextField(defaultValues[textFieldIndex]);
			} else {
				currentField = new TextField();
			}				
			
			textFields.addElement(currentField);
			
			add(new Label(inputNames[textFieldIndex]));
			add(currentField);
		}
		
		Panel buttonsPanel = new Panel();
		
		((Button)buttonsPanel.add(new Button("OK"))).addActionListener(new CloseWindowActionListener(this));
		((Button)buttonsPanel.add(new Button("Cancel"))).addActionListener(new CancelActionListener());
		
		add(buttonsPanel);
		
		pack();
	}
	
	public String getInput(int inputNumber) {
		String inputValue;
		
		if (inputNumber < textFields.size()) {
			inputValue = ((TextField)textFields.elementAt(inputNumber)).getText();
		} else {
			inputValue = null;
		}
		
		return inputValue;
	}
	
	class CancelActionListener implements ActionListener {
		public void actionPerformed(ActionEvent evt) {
			textFields.removeAllElements();
			InputDialog.this.dispose();
		}
	}
}
