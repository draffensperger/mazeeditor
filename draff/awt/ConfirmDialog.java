package draff.awt;

import java.awt.*;
import java.awt.event.*;
import draff.awt.event.*;

public class ConfirmDialog extends Dialog {
	public static final int CANCELED = 0;
	public static final int YES = 1;
	public static final int NO = 2;
	
	private int result = CANCELED;
	
	public ConfirmDialog(Frame parent, String title, String message) {
		super (parent);
		
		addWindowListener(new WindowCloseWindowListener());
		setTitle(title);
		setModal(true);
		
		setLayout(new GridLayout(2, 1));
		add(new Label(message));
		
		Panel buttonsPanel = new Panel();
		((Button)buttonsPanel.add(new Button("Yes"))).addActionListener(new YesActionListener());
		((Button)buttonsPanel.add(new Button("No"))).addActionListener(new NoActionListener());
		((Button)buttonsPanel.add(new Button("Cancel"))).addActionListener(new CancelActionListener());
		add(buttonsPanel);
		
		pack();
		setVisible(true);
	}
	
	class YesActionListener implements ActionListener {
		public void actionPerformed(ActionEvent evt) {
			result = YES;
			dispose();
		}
	}
	
	class NoActionListener implements ActionListener {
		public void actionPerformed(ActionEvent evt) {
			result = NO;
			dispose();
		}
	}
	
	class CancelActionListener implements ActionListener {
		public void actionPerformed(ActionEvent evt) {
			result = CANCELED;
			dispose();
		}
	}
	
	public int getResult() {
		return result;
	}
}
