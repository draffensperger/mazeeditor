package draff.awt.event;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class SystemExitActionListener implements ActionListener
{
	public void actionPerformed(ActionEvent event)
	{
		System.exit(0);
	}
}
