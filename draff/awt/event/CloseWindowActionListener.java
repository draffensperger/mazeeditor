package draff.awt.event;

import java.awt.event.*;
import java.awt.*;

public class CloseWindowActionListener implements ActionListener {
	Window windowToClose;
	
	public CloseWindowActionListener(Window windowToClose) {
		this.windowToClose = windowToClose;
	}
	
	public void actionPerformed(ActionEvent evt) {
		windowToClose.dispose();
	}
}
