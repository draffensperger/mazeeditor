package draff.awt.event;

import java.awt.event.*;
import java.awt.*;

public class SetCursorOnClick extends MouseAdapter	
{
	private Cursor cursorToSet;
	private Component cursorSetTarget;
	
	public SetCursorOnClick(Component cursorSetTarget, Cursor cursorToSet)
	{
		this.cursorSetTarget = cursorSetTarget;
		this.cursorToSet = cursorToSet;
	}
	
	public void mouseClicked(MouseEvent e) 
	{
		cursorSetTarget.setCursor(cursorToSet);
	}
}
