package draff.awt;

import java.awt.*;

public class SpinningLine extends Component implements Runnable {
	private Thread spinner;
	private Point currentPoint;
	private Point previousPoint;
	private double currentRadians;
	private double radianIncrement;
	private long delay;
		
	public SpinningLine(Color color, double radianIncrement, long delay) {
		spinner = new Thread(this);
		setForeground(color);
		currentPoint = new Point(0, 0);
		previousPoint = new Point(0, 0);
		currentPoint.x = (int)(Math.cos(currentRadians) * (getSize().width / 2) + getSize().width / 2);
		currentPoint.y = (int)(Math.sin(currentRadians) * (getSize().height / 2) + getSize().height / 2);
		currentRadians = 0.0;
		this.radianIncrement = radianIncrement;
		this.delay = delay;
		spinner.start();
	}
	
	public void run() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException err) {
		}
			
		
		while (true) {						
			try {
				Thread.sleep(delay);
			} catch (InterruptedException err) {
			}
			
			previousPoint.x = currentPoint.x;
			previousPoint.y = currentPoint.y;
			currentPoint.x = (int)(Math.cos(currentRadians) * (getSize().width / 2) + getSize().width / 2);
			currentPoint.y = (int)(Math.sin(currentRadians) * (getSize().height / 2) + getSize().height / 2);
			currentRadians = (currentRadians + radianIncrement ) % (2 * Math.PI);
			paint(getGraphics());
		}
	}
	
	public void update(Graphics g) {
	}
	
	public void paint(Graphics g) {			
		if (g == null) {
			return;
		}
		
		g.setXORMode(getBackground());
		g.setColor(getForeground());

		Rectangle clipRegion = new Rectangle();
		clipRegion.add(previousPoint);
		clipRegion.add(getSize().width / 2, getSize().height / 2);
		g.setClip(clipRegion);
		g.drawLine(getSize().width / 2, getSize().height / 2, previousPoint.x, previousPoint.y);

		clipRegion = new Rectangle();
		clipRegion.add(currentPoint.x, currentPoint.y);
		clipRegion.add(getSize().width / 2, getSize().height / 2);
		g.setClip(clipRegion);
		
		g.drawLine(getSize().width / 2, getSize().height / 2, currentPoint.x, currentPoint.y);
	}
}
