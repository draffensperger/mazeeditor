package draff.awt;

import java.net.URL;
import java.awt.Canvas;
import java.awt.Image;
import java.awt.Dimension;
import java.awt.MediaTracker;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.image.ImageObserver;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;

public class ImageCanvas extends Component {
	private transient Image image;
	private Dimension scale = null;
	private static int imageCanvasThatExist = 0;
	private String imageFileName = null;
	private boolean loadImageImmediatly;
	private static Hashtable imageTable = new Hashtable();
	
	public ImageCanvas() {
		this(null, true);
	}
	
	public ImageCanvas(String imageFileName) {
		this(imageFileName, true);
	}
	
	public ImageCanvas(String newImageFileName, boolean loadImageImmediatly) {
		this.loadImageImmediatly = loadImageImmediatly;
		if (loadImageImmediatly) {
			loadImage();
		}
		setImageFileName(newImageFileName);		
		setName(getClass().getName() + imageCanvasThatExist);
		imageCanvasThatExist++;
	}

	public ImageCanvas(Image image) {
		setImage(image);
		setName(getClass().getName() + imageCanvasThatExist);
	}

	public Image getImage() {
		return image;
	}
	
	public String getImageFileName () {
		return imageFileName;
	}
	
	public Dimension getScale() {
		return scale;
	}
	
	public void setScale(Dimension scale) {
		this.scale = scale;
	}
	
	public void loadImage() {
		try {
			while (!prepareImage(getImage(), this)) {
				try {
					Thread.sleep(250);
				} catch(InterruptedException err) {
				}
			}
		} catch (NullPointerException err) {
			err.printStackTrace();
		}
	}
	
	public Dimension getPreferredSize() {
		if (getImage() != null) {
			if (scale == null) {
				return new Dimension(getImage().getWidth(this), getImage().getHeight(this));
			} else {
				return new Dimension(scale.width, scale.height);
			}
		}

		return getMinimumSize();
	}
	
	public boolean imageUpdate(Image img, int flags, int x, int y, int w, int h) {
		if ((flags & ImageObserver.ERROR) != 0) {			
			System.out.println("Error loading image from file " + getImageFileName());
			return false;
		}
		
		if (((flags & ImageObserver.ALLBITS) != 0) && ((flags & ImageObserver.WIDTH) != 0) &&
			((flags & ImageObserver.HEIGHT) != 0)) {
			return false;
		}
		
		return true;
	}
	
	public void paint(Graphics g) {
		if (scale == null) {
			g.drawImage(getImage(), 0, 0, getSize().width, getSize().height, this);
		} else {
			g.drawImage(getImage(), 0, 0, scale.width, scale.height, this);
		}
	}
	
	public void setImage(Image image) {
		this.image = image;
		prepareImage(getImage(), this);	
	}	

	public void setImageFileName(String newImageFileName) {
		this.imageFileName = newImageFileName;
		
		if (newImageFileName != null) {			
			if (imageTable.containsKey(newImageFileName)) {
				setImage((Image)imageTable.get(newImageFileName));
			} else {
				try {
					setImage(MediaSupplier.getImage(newImageFileName));
				} catch (Exception err) {
					err.printStackTrace();
				}
				
				imageTable.put(newImageFileName, getImage());
			}
		}
	}
	
	/**
	 * @param c The component for which the images are being tracked.
	 */
	public static void loadAllImages(Component c) {
		Enumeration images = imageTable.elements();
		MediaTracker tracker = new MediaTracker(c);
		
		int id = 0;
		while (images.hasMoreElements()) {
			tracker.addImage((Image)images.nextElement(), id++);
		}
		
		try {
			tracker.waitForAll();
		} catch (InterruptedException interrupted) {}
	}
	
	/**
	 * Add the image at the spesified filename to the list that
	 * can be loaded when loadAddImages is called.
	 */
	public static void registerFileName(String fileName) {
		imageTable.put(fileName, MediaSupplier.getImage(fileName));
	}
	
	private void writeObject(java.io.ObjectOutputStream out)
		throws IOException 
	{
		out.defaultWriteObject();
	}
	
	private void readObject(java.io.ObjectInputStream in)
		throws IOException, ClassNotFoundException
	{
		in.defaultReadObject();
		
		// 'getImageFileName() != null' must come BEFORE '!getImageFileName().equals("")',
		// otherwise a 'NullPointerException' may occur if getImageFileName() returned 'null'
		if (getImageFileName() != null && !getImageFileName().equals("")) {
			// Although the file name is already set setImageFileName assigns and possibly loads the real image.
			setImageFileName(getImageFileName());
		}
	}
}