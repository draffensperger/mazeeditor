package draff.awt;

import java.applet.*;
import java.awt.*;
import java.io.*;
import java.net.*;
import java.util.zip.*;
import java.util.*;

public class MediaSupplier {
	private static Applet appletParent = null;
	private static InputStream archive = null;
	private static Hashtable markedZipInputStreams = new Hashtable();
	
	public static void initilize(Applet appletParent, String archive) throws
			MalformedURLException, FileNotFoundException, IOException {
		MediaSupplier.appletParent = appletParent;
		
		if (archive != null) {
			MediaSupplier.archive = getNonZipInputStream(archive);
		}
	}
	
	public static Image getImage(String fileName) {
		if (appletParent != null) {
			return appletParent.getImage(appletParent.getCodeBase(), fileName);
		} else {
			return Toolkit.getDefaultToolkit().getImage(fileName);
		}
	}
	
	public static AudioClip getAudio(String fileName) {
		if (appletParent != null) {
			return appletParent.getAudioClip(appletParent.getCodeBase(), fileName);
		} else {
			return null;
		}
	}
	
	public static InputStream getInputStream(String fileName) throws MalformedURLException,
				FileNotFoundException, IOException {
		if (archive != null) {
			try {
				return getZipInputStream(archive, fileName);
			} catch (Exception zipStreamAttemptFailed) {
				zipStreamAttemptFailed.printStackTrace();
			}
		}
		
		return getNonZipInputStream(fileName);
	}
	
	public static ZipInputStream getZipInputStream(String fileName, String entry) throws IOException, 
			FileNotFoundException, ZipException {
		return getZipInputStream(getNonZipInputStream(fileName), entry);
	}
	
	public static ZipInputStream getZipInputStream(InputStream stream, String entry) throws 
				IOException, FileNotFoundException, ZipException {
		ZipInputStream zipStream;
		
		if (markedZipInputStreams.containsKey(stream)) {
			//zipStream = markedZipInputStreams.get(stream);
			zipStream = null;
		} else {
			zipStream = new ZipInputStream(stream);
			//zipStream.mark();
		}
		
		try {
			while (!zipStream.getNextEntry().getName().equals(entry)) {
				zipStream.closeEntry();
			}
			
			return zipStream;
		} catch (NullPointerException lastEntryNameWasReached) {
			throw new FileNotFoundException(entry);
		}
	}
	
	private static InputStream getNonZipInputStream(String fileName) throws
			MalformedURLException, FileNotFoundException, IOException {
		if (appletParent != null) {
			return new URL(appletParent.getDocumentBase(), fileName).openStream();
		} else {
			return new FileInputStream(fileName);
		}
	}
}
