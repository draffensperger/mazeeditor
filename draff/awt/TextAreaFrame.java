package draff.awt;

import draff.awt.event.WindowCloseWindowListener;
import java.awt.TextArea;
import java.awt.Frame;

public class TextAreaFrame extends Frame
{
	public TextAreaFrame(String title, String whatToPutInTextArea)
	{
		setTitle(title);

		addWindowListener(new WindowCloseWindowListener());

		TextArea textDisplayer = new TextArea(whatToPutInTextArea);
		textDisplayer.setEditable(false);
		add(textDisplayer, "Center");
	}
}