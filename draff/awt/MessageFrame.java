package draff.awt;

import draff.awt.event.*;
import java.awt.*;

public class MessageFrame extends Frame {
	public MessageFrame(String title, String text) {				
		addWindowListener(new WindowCloseWindowListener());
		add(new Label(text));
		setTitle(title);
		setVisible(true);
		pack();
	}	
}
