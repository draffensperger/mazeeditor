package draff.awt;

import draff.io.RelativeDirectoryParser;
import java.io.*;
import java.awt.*;
import java.awt.event.*;

public class FileTextField extends Container {
	private TextField textField;
	private Button browseButton;
	private String browseDialogTitle;
	private FilenameFilter filenameFilter;
	private Frame browseDialogParent;
	
	private static final String DEFAULT_BROWSE_BUTTON_CAPTION = "Browse...";
	private static final String DEFAULT_BROWSE_DIALOG_TITLE = "Select a File";
	
	public FileTextField() {
		this ("", DEFAULT_BROWSE_BUTTON_CAPTION, DEFAULT_BROWSE_DIALOG_TITLE, new Frame(), null);
	}
	
	public FileTextField(String startingText, String browseButtonCaption, String browseDialogTitle, Frame browseDialogParent, 
						 FilenameFilter filenameFilter) {
		
		setLayout(new FlowLayout());		
		textField = new TextField(startingText);		
		add(textField);
		
		browseButton = new Button(browseButtonCaption);
		browseButton.addActionListener(new BrowseActionListener());
		add(browseButton);
		
		this.browseDialogTitle = browseDialogTitle;
		this.browseDialogParent = browseDialogParent;
		this.filenameFilter = filenameFilter;
	}
	
	public void setText(String text) {
		textField.setText(text);
	}
	
	public String getText() {
		return textField.getText();
	}
	
	public void addTextListener(TextListener listener) {
		textField.addTextListener(listener);
	}
	
	public void setColumns(int columns) {
		textField.setColumns(columns);
	}
	
	class BrowseActionListener implements ActionListener {
		public void actionPerformed(ActionEvent evt) {
			FileDialog browseDialog = new FileDialog(browseDialogParent, browseDialogTitle, FileDialog.LOAD);
			browseDialog.setFilenameFilter(filenameFilter);
			browseDialog.pack();
			browseDialog.setVisible(true);
					
			if (browseDialog.getFile() != null && !browseDialog.getFile().equals("")) {
				textField.setText(RelativeDirectoryParser.getRelativeDirectory(browseDialog.getDirectory(),
					(new File("")).getAbsolutePath()) + browseDialog.getFile());
			}
		}
	}
}
