package draff.io;

import java.util.StringTokenizer;
import java.io.File;

public class RelativeDirectoryParser {
	/**
	* This method takes two directories, and a 
	*/	
	public static String getRelativeDirectory(String fullCurrentDirectory, String fullRelativeDirectory) {
		if (fullCurrentDirectory.equals(fullRelativeDirectory)) {
			return fullCurrentDirectory;		
		} else {
			StringTokenizer currentDirTokenizer = new StringTokenizer(fullCurrentDirectory, File.separator, false);
			StringTokenizer relativeDirTokenizer = new StringTokenizer(fullRelativeDirectory, File.separator, false);
			
			if (currentDirTokenizer.nextToken().equals(relativeDirTokenizer.nextToken())) {
				StringBuffer resultBuffer = new StringBuffer();
				
				String lastRelativeDirToken = null;
				
				while (currentDirTokenizer.hasMoreTokens() && relativeDirTokenizer.hasMoreTokens()) {
					String currentRelativeDirToken = relativeDirTokenizer.nextToken();
					if (!currentDirTokenizer.nextToken().equals(currentRelativeDirToken)) {
						resultBuffer.append("..");
						lastRelativeDirToken = currentRelativeDirToken;
						break;
					}
				}				
				
				if (currentDirTokenizer.hasMoreTokens()) {					
					while (currentDirTokenizer.hasMoreTokens()) {
						resultBuffer.append(".");
						currentDirTokenizer.nextToken();
					}					
				} 
				
				if (lastRelativeDirToken != null) {
					resultBuffer.append("/" + lastRelativeDirToken + "/");
				}
				
				while (relativeDirTokenizer.hasMoreTokens()) {					
					resultBuffer.append(relativeDirTokenizer.nextToken());
					
					if (relativeDirTokenizer.hasMoreTokens()) {
						resultBuffer.append("/");
					}
				}	
				
				resultBuffer.append("/");
				return new String(resultBuffer);
			} else {
				return fullRelativeDirectory;
			}
		}
	}
}
