package mazegame;

import java.awt.*;
import java.awt.event.*;
import java.applet.*;
import java.util.*;
import java.io.*;
import java.net.*;
import java.lang.reflect.Method;
import draff.awt.*;
import draff.awt.event.*;

public class GameMenu extends MenuBar {
	private MazeContainer mazeContainerParent = null;
	public static Properties programInfo;	
	private String missionFileName;
	private Thread loadGameThread;
	private TextArea errorMessageTextArea;
	private Panel errorMessageDisplay;
	
	public GameMenu(MazeContainer mazeContainerParent, String startupMissionToLoad) {
		this.mazeContainerParent = mazeContainerParent;
		
		errorMessageDisplay = new Panel();
		errorMessageDisplay.setLayout(new BorderLayout());
		Label errorLabel = new Label("Error Loading Game", Label.CENTER);
		errorLabel.setFont(new Font("SanSerif", Font.BOLD, 
				Integer.parseInt(MazeContainer.programInfo.getString("loading.error.labelfontsize"))));
		errorMessageDisplay.add(errorLabel, "North");
			
		errorMessageTextArea = new TextArea();
		errorMessageDisplay.add(errorMessageTextArea, "Center");
			
		mazeContainerParent.addComponentToMainArea(errorMessageDisplay, errorMessageDisplay.getName());
				
		Menu game = new Menu("Game");		

		(game.add(new MenuItem("Open Maze"))).addActionListener(new BrowseForNewGameActionListner(mazeContainerParent));
		game.addSeparator();

		add(game);

		Menu help = new Menu("Help");

		MenuItem howToPlay = new MenuItem(MazeContainer.programInfo.getString("help.howtoplay.menucaption"));
		howToPlay.addActionListener(new DisplayTextFileActionListener(MazeContainer.programInfo.getString(
				"help.howtoplay.filename"), new Font("Courier", Font.PLAIN, 12)));
		help.add(howToPlay);

		add(help);
		
		if (startupMissionToLoad == null || startupMissionToLoad.equals("")) {		
			addMissionActivatorMenus(game);
		} else {
			(new MazeLoaderThread(startupMissionToLoad, mazeContainerParent)).start();
		}
	}
	
	private void addMissionActivatorMenus(Menu destinationMenu) {
		try {
			StringTokenizer missionFileNames = 
					new StringTokenizer(MazeContainer.programInfo.getString("missions"), ";", false);
			StringTokenizer missionCaption = 
					new StringTokenizer(MazeContainer.programInfo.getString("missions.captions"), ";", false);
			MenuItem missionActivator;
			while (missionFileNames.hasMoreTokens()) {				
				missionActivator = new MenuItem(missionCaption.nextToken());
				missionActivator.addActionListener(new NewGameActionListener(MazeContainer.programInfo.getString("missions.directory") 
																			 + "/" + missionFileNames.nextToken(), mazeContainerParent));
				destinationMenu.add(missionActivator);
			}		
		} catch (Exception err) {
			err.printStackTrace();
		}
	}
	
	class NewGameActionListener implements ActionListener {
		private String mazeFileName;
		private MazeContainer mazeContainerParent;
		
		public NewGameActionListener(String mazeFileName, MazeContainer mazeContainerParent) {
			this.mazeContainerParent = mazeContainerParent;
			this.mazeFileName = mazeFileName;
		}
		
		public void actionPerformed(ActionEvent evt) {
			(new MazeLoaderThread(mazeFileName, mazeContainerParent)).start();			
		}
	}
	
	class BrowseForNewGameActionListner implements ActionListener {
		private MazeContainer mazeContainerParent;
		
		public BrowseForNewGameActionListner(MazeContainer mazeContainerParent) {
			this.mazeContainerParent = mazeContainerParent;
		}
		
		public void actionPerformed(ActionEvent evt) {
			FileDialog openMazeFileDialog = new FileDialog(new Frame(), "Select a Maze to Play", FileDialog.LOAD);
			openMazeFileDialog.pack();
			openMazeFileDialog.setVisible(true);
			String mazeFileName = openMazeFileDialog.getDirectory() + "/" + openMazeFileDialog.getFile();
			
			(new MazeLoaderThread(mazeFileName, mazeContainerParent)).start();
		}
	}
	
	class MazeLoaderThread extends Thread {
		private String mazeFileName;
		private MazeContainer mazeContainerParent;
		
		public MazeLoaderThread(String mazeFileName, MazeContainer mazeContainerParent) {			
			this.mazeContainerParent = mazeContainerParent;
			this.mazeFileName = mazeFileName;
			
			setName("Loader for maze: " + mazeFileName);
			setPriority(Thread.MAX_PRIORITY);
		}
		
		public void run() {
			try {
				mazeContainerParent.showLoadingScreen();
				Maze mission = getLoadedMaze(mazeFileName);

				mazeContainerParent.showGameScreen(mission);
								
				try {
					mazeContainerParent.list(new PrintStream(new FileOutputStream("mzout.txt")));
				} catch (Exception err) {
					err.printStackTrace();
				}
			} catch (Exception err) {
				StringWriter messageWriter = new StringWriter();
				err.printStackTrace(new PrintWriter(messageWriter));
				errorMessageTextArea.setText(messageWriter.toString());
				
				mazeContainerParent.showComponent(errorMessageDisplay);
			}
		}
	}
		
	private Maze getLoadedMaze(String missionFileName) throws IOException, ClassNotFoundException {
		Maze mission = (Maze)(new ObjectInputStream(MediaSupplier.getInputStream(missionFileName)).readObject());
		return mission;
	}
}