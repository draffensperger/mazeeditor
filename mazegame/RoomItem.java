package mazegame;

import draff.awt.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;

/**
 * An item in a room.
 * @see Room
 * @author David Raffensperger
 * @version 1.0 13 April 1999
 */
public class RoomItem extends ImageCanvas implements MouseListener, MouseMotionListener {
	private Maze maze;
	private String normalImageFile;
	private String mouseOverImageFile;
	private Point normalLocation;
	private Point mouseOverLocation;
	private boolean changeImageWhenMouseIsOver;
	
	public RoomItem(String normalImageFile, Point normalLocation) {
		this (normalImageFile, normalLocation, null, null);
		this.changeImageWhenMouseIsOver = false;
	}
	
	// NOTE: Both points are in the parents coordinate system
	public RoomItem(String normalImageFile, Point normalLocation, String mouseOverImageFile, 
					Point mouseOverLocation) {
		super (normalImageFile, true);
		
		this.normalImageFile = normalImageFile;
		this.normalLocation = normalLocation;
		this.mouseOverImageFile = mouseOverImageFile;
		this.mouseOverLocation = mouseOverLocation;
		this.changeImageWhenMouseIsOver = true;

		setLocation(normalLocation);

		addMouseListener(this);
		addMouseMotionListener(this);
	}
	
	public void setMaze(Maze maze) {
		this.maze = maze;
		setSize(getPreferredSize());
	}
	
	public Maze getMaze() {
		return maze;
	}
		
	public void mouseEntered(MouseEvent e) {
		if (changeImageWhenMouseIsOver) {
			setImageFileName(mouseOverImageFile);
			loadImage();
			setBoundsScaled(mouseOverLocation, getPreferredSize());
		}
	}
	
	public void mouseExited(MouseEvent e) {
		if (changeImageWhenMouseIsOver) {
			setImageFileName(normalImageFile);
			loadImage();
			setBoundsScaled(normalLocation, getPreferredSize());
		}
	}
	
	public void mousePressed(MouseEvent e) {}
	public void mouseReleased(MouseEvent e)  {}	
	public void mouseClicked(MouseEvent e) {}
	
	public void mouseMoved(MouseEvent e) {}
	public void mouseDragged(MouseEvent e) {}
	
	public void setNormalImageFileName(String fileName) {
		this.normalImageFile = fileName;
	}
	
	private void setBoundsScaled(Point pt, Dimension size) { 
		((ScaleLayout)getParent().getLayout()).setStartBounds(
				this, new Rectangle(pt.x, pt.y, size.width, size.height));
		getParent().doLayout();
	}
}