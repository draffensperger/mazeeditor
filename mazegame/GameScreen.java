// GameScreen.java

package mazegame;

import java.awt.*;

/**
 * It is the job of the game screen to display a maze thru a room display and an inventory
	display.
 *
 * @author David Raffensperger
 * @version 0.9
 */
public class GameScreen extends MazeViewerPanel
{
	/** The maze to display */
	private Maze maze;
	private RoomDisplay roomScreen;
	private InventoryDisplay inventoryScreen;
	private static int numberOfTheeseThatExist = 0;

	/**
	 * Constructs a GameScreen.
	 * The room display should be above the inventory display, they should be the same width,
	 * and they should be their perferred size.
	 */
	public GameScreen(Maze maze)
	{
		super(maze);
		
		setName(getClass().getName() + numberOfTheeseThatExist);
		numberOfTheeseThatExist++;
						  
		roomScreen = new RoomDisplay(maze);
		inventoryScreen = new InventoryDisplay(maze);

		setLayout(new BorderLayout());
				
		add(roomScreen);
		add(inventoryScreen, "South");
				
		setVisible(true);
	}

	public void mazeStateChanged(Maze previousState) {
		validate();
	}
}

// GameScreen.java