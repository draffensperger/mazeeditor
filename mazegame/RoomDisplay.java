package mazegame;

import java.awt.*;

public class RoomDisplay extends MazeViewerPanel {	
	public RoomDisplay(Maze maze) {
		super (maze);

		setLayout(new CardLayout());
		
		for(int roomCount = 0; roomCount < getMaze().getRooms().length; roomCount++) {
			add(getMaze().getRooms()[roomCount], getMaze().getRooms()[roomCount].getName());
		}

		// index 0 is the starting room
		showComponent(getMaze().getRooms()[0]);
		setVisible(true);
	}

	public void setMaze(Maze maze) {
		super.setMaze(maze);
	}
	
	public void mazeStateChanged(Maze previousState) {
		if (previousState == null || getMaze().getQuester().getRoomLocatedIn() != 
									 previousState.getQuester().getRoomLocatedIn()) {
			showComponent(getMaze().getRooms()[getMaze().getQuester().getRoomLocatedIn()]);
		}
	}

	public void showComponent(Component whatToShow) {
		((CardLayout)getLayout()).show(this, whatToShow.getName());
	}
}