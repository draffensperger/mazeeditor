package mazegame;

import draff.awt.event.*;
import draff.awt.*;
import java.applet.Applet;
import java.awt.*;
import java.io.*;
import java.util.*;
import java.net.*;

public class MazeContainer extends MazeViewerPanel implements Runnable {
	private CardLayout gameAreaLayout;
	private Container gameArea;
	private Container menuArea;
	private GameScreen gameScreen;
	private ImageCanvas background;
	private ImageCanvas lostBackground;
	private ImageCanvas wonBackground;
	private Panel loadingGamePanel;
	
	private String defaultBackground = "images/background.jpg";		
	public static final long REPAINT_DELAY = 100;
	public static PropertyResourceBundle programInfo;
	
	public MazeContainer(String startupMaze)
	{		
		super (null);
		
		try {
			programInfo = new PropertyResourceBundle(MediaSupplier.getInputStream("mazegame.properties"));
		}
		catch(Exception err) {
			System.out.println("Unable to load program info:");
			err.printStackTrace();
		}
		
		setLayout(new BorderLayout(0, 0));

		gameArea = new LightWeightPanel();
		gameAreaLayout = new CardLayout();
		gameArea.setLayout(gameAreaLayout);

		background = new ImageCanvas(defaultBackground);
		gameArea.add(background.getName(), background);
		showComponent(background);
		
		ImageCanvas loadingBackground = new ImageCanvas("images/loading.jpg", true);
		loadingBackground.loadImage();
		loadingBackground.setSize(loadingBackground.getPreferredSize());
		loadingGamePanel = new Panel();		
		loadingGamePanel.setLayout(new ScaleLayout(loadingBackground.getPreferredSize()));
		loadingGamePanel.add(loadingBackground, loadingBackground.getBounds());
		SpinningLine clockHand = new SpinningLine(Color.red, Math.PI / 7, 400);
		clockHand.setLocation(173, 86);
		clockHand.setSize(150, 150);
		loadingGamePanel.add(clockHand, clockHand.getBounds());
		gameArea.add(loadingGamePanel.getName(), loadingGamePanel);
		
		menuArea = new MenuBarPanel(new GameMenu(this, startupMaze));
		
		lostBackground = new ImageCanvas("images/backgroundlost.jpg");
		gameArea.add(lostBackground, lostBackground.getName());
		
		wonBackground = new ImageCanvas("images/backgroundwon.jpg");
		gameArea.add(wonBackground, wonBackground.getName());
		
		add(menuArea, "North");
		add(gameArea, "Center");
				
		setVisible(true);
	}

	public void mazeStateChanged(Maze previousState) {
		// May possiblly help to stablize memory usage.
		System.gc();
		
		if (previousState == null || 
			!getMaze().getRooms()[getMaze().getQuester().getRoomLocatedIn()].getProgramBackground().equals(
			previousState.getRooms()[getMaze().getQuester().getRoomLocatedIn()].getProgramBackground())) {
			
			getParent().setBackground(
					getMaze().getRooms()[getMaze().getQuester().getRoomLocatedIn()].getProgramBackground());
			getParent().repaint(MazeContainer.REPAINT_DELAY);
		}
	
		if (getMaze().isGameOver()) {				
			// This must be in a second thread because this thread is the one that displays
			// the components.
			Thread gameOver = new Thread(this);
			gameOver.start();
		}
	}

	public void run() {
		if (getMaze().getWasVictoryAchieved()) {
			showComponent(wonBackground);
		} else {
			showComponent(lostBackground);
		}
		
		try {
			Thread.sleep(2000);
		} catch (Exception err) {
			err.printStackTrace();
		}						
							
		showComponent(background);
	}
	
	private GameScreen previousGameScreen = null;
	public void showGameScreen(Maze maze) {		
		ImageCanvas.loadAllImages(this);
		if (previousGameScreen != null) {
 			gameArea.remove(previousGameScreen);
		}
		
		gameScreen = new GameScreen(maze);
		previousGameScreen = gameScreen;
		
		gameArea.add(gameScreen, gameScreen.getName());
	
		setMaze(maze);
			
		showComponent(gameScreen);
		maze.startGame();
	}
	
	public void showLoadingScreen() {
		showComponent(loadingGamePanel);
	}
	
	public void addComponentToMainArea(Component comp, String constraint) {
		gameArea.add(comp, constraint);
	}
	
	public void showComponent(Component whatToShow)	{
		gameAreaLayout.show(gameArea, whatToShow.getName());
	}
}				 