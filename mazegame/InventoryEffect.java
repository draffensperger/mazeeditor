package mazegame;

/**
 * An InventoryEffect is the effect a particular InventoryItem will have on a RoomItem, if the 
 * inventory is acted on the RoomItem.
 */
public abstract class InventoryEffect implements java.io.Serializable {
	private String message;
	private InventoryItem inventory;
	
	public InventoryEffect(InventoryItem inventory) {
		this (inventory, null);
	}
	
	public InventoryEffect(InventoryItem inventory, String message) {
		setMessage(message);
		this.inventory = inventory;
	}
	
	public InventoryItem getInventory() {
		return null;
	}
	
	public String getMessage() {
		return message;	
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public abstract void performEffect();
}
