package mazegame;

import draff.awt.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class Room extends MazeViewerPanel implements Cloneable {
	private Color programBackground = null;
	private ImageCanvas bgDisplayer;
	private Vector itemsInRoom;
	private String bgDisplayerImageFile;
	private static int roomThatExist = 0;
	private Point floorPlanLocation;
	
	// It's necessary to make itemsPanel an ItemsDisplayer instead of just a Panel,
	// because instances of Panel are not light weight, but ItemsDisplayer is.
	private LightWeightPanel itemsPanel;
	
	public Room(String imageFile, Color programBackground) {
		super (null);

		setProgramBackground(programBackground);
		
		bgDisplayerImageFile = imageFile;
		setLayout(new AllComponentsMaxSizeLayout());
		setName(getClass().getName() + roomThatExist);
		roomThatExist++;
		
		itemsInRoom = new Vector();
	}
		
	public void setMaze(Maze maze) {
		super.setMaze(maze);
		
		if (maze != null) {
			removeAll();
			
			itemsPanel = new LightWeightPanel();
			bgDisplayer = new ImageCanvas(bgDisplayerImageFile);
			
			// The background image must be loaded if the ScaleLayout below is to have a meaningful starting scale size.
			bgDisplayer.loadImage();
			itemsPanel.setLayout(new ScaleLayout(bgDisplayer.getPreferredSize()));
		
			add(itemsPanel);
			add(bgDisplayer);
		}
	}
	
	public String getBackgroundImageFileName() {
		return bgDisplayerImageFile;
	}
	
	public void setBackgroundImageFileName(String imageFileName) {
		this.bgDisplayerImageFile = imageFileName;
		
		if (getMaze() != null) {			
			bgDisplayer.setImageFileName(imageFileName);
			bgDisplayer.loadImage();
			bgDisplayer.setSize(bgDisplayer.getPreferredSize());
			bgDisplayer.repaint();
		}
	}
	
	public Color getProgramBackground() {
		return programBackground;
	}
	
	public void setProgramBackground(Color background) {
		this.programBackground = background;
	}
	
	public Vector getItemsInRoom() {
		return itemsInRoom;
	}
	
	public void setItemsInRoom(Vector itemsInRoom) {
		this.itemsInRoom = itemsInRoom;
	}

	public void addRoomItem(RoomItem item) {		
		itemsPanel.add(item);	
		itemsInRoom.addElement(item);
		itemsPanel.validate();		
	}

	public void removeRoomItem(RoomItem item) {
		itemsInRoom.removeElement(item);
		itemsPanel.remove(item);
		itemsPanel.validate();
		itemsPanel.repaint();
	}

	public void mazeStateChanged(Maze previousState) {
		
	}
	
	public void setFloorPlanLocation(Point floorPlanLocation) {
		this.floorPlanLocation = floorPlanLocation;
	}
	
	public Point getFloorPlanLocation() {
		return floorPlanLocation;
	}
	
	public Object clone() throws CloneNotSupportedException {
		Room clone = (Room)super.clone();
		
		clone.programBackground = new Color(programBackground.getRGB());
		clone.itemsInRoom = (Vector)itemsInRoom.clone();

		/*LightWeightPanel cloneItemsPanel = new LightWeightPanel();
		
		for (int itemIndex = 0; itemIndex < itemsInRoom.size(); itemIndex++) {
			cloneItemsPanel.add((RoomItem)itemsInRoom.elementAt(itemIndex));
		}
		
		clone.itemsPanel = cloneItemsPanel;*/
		
		return clone;
	}
}