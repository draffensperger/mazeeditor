package mzeditor;

import java.awt.*;
import java.awt.event.*;
import mazegame.*;
import draff.awt.*;
import draff.awt.event.*;
import draff.lang.reflect.ExpressionDecoder;
import draff.io.*;
import java.io.File;
import java.lang.reflect.*;
import java.util.*;

public class RoomEditingControls extends Panel {
	private Room selectedRoom;
	private FloorPlanRoom floorPlanRoom;

	private Label floorPlanLocation;
	private FileTextField backgroundImageText;
	private Panel backgroundImagePreviewPanel;
	private ImageCanvas backgroundImagePreview;
	private Label noImageFoundLabel;
	private TextField red;
	private TextField green;
	private TextField blue;
	
	private TextField roomItemConstructorText;
	
	private Maze mazeParent;
	
	public RoomEditingControls(Maze mazeParent) {
		this.mazeParent = mazeParent;
												
		setLayout(new GridLayout(3, 1, 0, 0));
		
		Panel row1 = new Panel();
		
		floorPlanLocation = new Label();
		row1.add(floorPlanLocation);
		row1.add(new Label("Background"));
		
		backgroundImageText = new FileTextField();
		backgroundImageText.setColumns(25);
		backgroundImageText.addTextListener(new BgImageTextListener());		
		row1.add(backgroundImageText);
		
		backgroundImagePreviewPanel = new Panel();
		backgroundImagePreviewPanel.setLayout(new CardLayout());
		
		backgroundImagePreview = new ImageCanvas();
		backgroundImagePreview.setScale(new Dimension(50, 50));		
		backgroundImagePreviewPanel.add(backgroundImagePreview, backgroundImagePreview.getName());
		
		noImageFoundLabel = new Label("No image found");
		backgroundImagePreviewPanel.add(noImageFoundLabel, noImageFoundLabel.getName());
			
		row1.add(backgroundImagePreviewPanel);
		
		add(row1);
		
		Panel row2 = new Panel();
		
		Panel backgroundColorControls = new Panel();
		backgroundColorControls.add(new Label("Background color: "));
		
		backgroundColorControls.add(new Label("Red"));
		red = new TextField();
		red.addTextListener(new RedChangedTextListener());
		backgroundColorControls.add(red);
		
		backgroundColorControls.add(new Label("Green"));
		green = new TextField();
		green.addTextListener(new GreenChangedTextListener());
		backgroundColorControls.add(green);
		
		backgroundColorControls.add(new Label("Blue"));
		blue = new TextField();
		blue.addTextListener(new BlueChangedTextListener());
		backgroundColorControls.add(blue);
		
		row2.add(backgroundColorControls);
		
		add(row2);
		
		Panel row3 = new Panel();		
		
		row3.add(new Label("Constructor call"));
		roomItemConstructorText = new TextField(20);
		row3.add(roomItemConstructorText);
		((Button)row3.add(new Button("Add Room Item"))).addActionListener(new AddRoomItemActionListener());		
		((Button)row3.add(new Button("Remove Room Item"))).addActionListener(new RemoveRoomItemActionListener());		
		((Button)row3.add(new Button("View Room"))).addActionListener(new ViewRoomActionListener());
				
		add(row3);	
	}
	
	public Room getSelectedRoom() {
		return selectedRoom;
	}
	
	public void setRoom(Room room, FloorPlanRoom floorPlanRoom) {
		this.selectedRoom = room;
		this.floorPlanRoom = floorPlanRoom;
		
		floorPlanLocation.setText("X: " + room.getFloorPlanLocation().x + "  Y:" + room.getFloorPlanLocation().y);
		backgroundImageText.setText(room.getBackgroundImageFileName());
		backgroundImagePreview.setImageFileName(room.getBackgroundImageFileName());
		backgroundImagePreview.setBackground(room.getProgramBackground());
		backgroundImagePreview.loadImage();
		backgroundImagePreview.repaint();
		red.setText(String.valueOf(room.getProgramBackground().getRed()));
		green.setText(String.valueOf(room.getProgramBackground().getGreen()));
		blue.setText(String.valueOf(room.getProgramBackground().getBlue()));
		
		validate();
	}
	
	class BgImageTextListener implements TextListener {
		public void textValueChanged(TextEvent evt) {
			String currentAbsolutePath = (new File("")).getAbsolutePath();
			
			if ((new File(((TextComponent)evt.getSource()).getText())).exists()) {
				if (!selectedRoom.getBackgroundImageFileName().equals(((TextComponent)evt.getSource()).getText())) {
					selectedRoom.setBackgroundImageFileName(((TextComponent)evt.getSource()).getText());
					mazeParent.setChanged();
					
					backgroundImagePreview.setImageFileName(((TextComponent)evt.getSource()).getText());
					backgroundImagePreview.loadImage();
					backgroundImagePreview.repaint();
					((CardLayout)backgroundImagePreviewPanel.getLayout()).show(backgroundImagePreviewPanel, backgroundImagePreview.getName());
			
					floorPlanRoom.updateComponents();
				}
			} else {
				((CardLayout)backgroundImagePreviewPanel.getLayout()).show(backgroundImagePreviewPanel, noImageFoundLabel.getName());
			}
		}
	}
	
	class RedChangedTextListener implements TextListener {
		public void textValueChanged(TextEvent evt) {
			Color oldColor = selectedRoom.getProgramBackground();
			
			try {			
				Color newColor = new Color(Integer.parseInt(((TextComponent)evt.getSource()).getText()), 
										   oldColor.getGreen(), oldColor.getBlue());
				
				if (!oldColor.equals(newColor)) {
					selectedRoom.setProgramBackground(newColor);
					mazeParent.setChanged();
					floorPlanRoom.updateComponents();
				}
			} catch (NumberFormatException badInput) {
				badInput.printStackTrace();
			}
		}
	}
	
	class GreenChangedTextListener implements TextListener {
		public void textValueChanged(TextEvent evt) {
			Color oldColor = selectedRoom.getProgramBackground();
			
			try {			
				Color newColor = new Color(oldColor.getRed(), 
						   Integer.parseInt(((TextComponent)evt.getSource()).getText()), oldColor.getBlue());
				
				if (!oldColor.equals(newColor)) {
					selectedRoom.setProgramBackground(newColor);
					floorPlanRoom.updateComponents();
					mazeParent.setChanged();
				}
			} catch (NumberFormatException badInput) {
				badInput.printStackTrace();
			}
		}
	}
	
	class BlueChangedTextListener implements TextListener {
		public void textValueChanged(TextEvent evt) {
			Color oldColor = selectedRoom.getProgramBackground();
			
			try {			
				Color newColor = new Color(oldColor.getRed(), oldColor.getGreen(), 
										   Integer.parseInt(((TextComponent)evt.getSource()).getText()));
				
				if (!oldColor.equals(newColor)) {
					selectedRoom.setProgramBackground(newColor);
					floorPlanRoom.updateComponents();
					mazeParent.setChanged();
				}
			} catch (NumberFormatException badInput) {
				badInput.printStackTrace();
			}
		}
	}
	
	class ViewRoomActionListener implements ActionListener {
		public void actionPerformed(ActionEvent evt) {
			floorPlanRoom.showViewFrame();
		}
	}
	
	class AddRoomItemActionListener implements ActionListener {
		public void actionPerformed(ActionEvent evt) {
			Dialog whereILeftOFF = new AddRoomItemDialog(new Frame());
			whereILeftOFF.setVisible(true);
			whereILeftOFF.pack();
			
			try {
				RoomItem itemToAdd = (RoomItem)ExpressionDecoder.constructObject(roomItemConstructorText.getText());
				itemToAdd.setMaze(mazeParent);
				itemToAdd.loadImage();
				itemToAdd.setSize(itemToAdd.getPreferredSize());
				selectedRoom.addRoomItem(itemToAdd);
				floorPlanRoom.updateComponents();
				mazeParent.setChanged();
			} catch (Exception exception) {
				new MessageFrame("An Exception Occurred", "An exception occurred when you tried to add a RoomItem: " +
														  exception.getMessage());
			}
		}
	}
	
	
	class RemoveRoomItemActionListener implements ActionListener {
		public void actionPerformed(ActionEvent evt) {
			floorPlanRoom.removeSelectedRoomItem();
			mazeParent.setChanged();
		}
	}
}
