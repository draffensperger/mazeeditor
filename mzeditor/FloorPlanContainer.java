package mzeditor;

import draff.awt.*;
import java.awt.*;
import java.awt.event.*;
import mazegame.*;
import mazecomponents.*;
import java.util.*;

public class FloorPlanContainer extends Panel {
	private Maze maze;
	private Hashtable floorPlanRoomsOfRealRooms;
	
	private RoomsContainer roomsContainer;
	private Panel mazeInformationPanel;
	private TextField mazeName;
	private ChangeMazeNameTextListener changeMazeNameListener;
	private RoomEditingControls mazeRoomControls;	
	
	public FloorPlanContainer() {
		setLayout(new BorderLayout());
		
		mazeInformationPanel = new Panel();
		add(mazeInformationPanel, "North");
		
		mazeName = new TextField(20);
		mazeInformationPanel.add(mazeName);
		changeMazeNameListener = new ChangeMazeNameTextListener();
		mazeName.addTextListener(changeMazeNameListener);
		
		roomsContainer = new RoomsContainer();
		roomsContainer.setBackground(Color.white);	
		
		add(roomsContainer, "Center");
	}
	
	public void setMaze(Maze maze) {
		if (mazeRoomControls != null) {
			remove(mazeRoomControls);
		}
		
		mazeRoomControls = new RoomEditingControls(maze);
		add(mazeRoomControls, "South");
		
		this.maze = maze;
		floorPlanRoomsOfRealRooms = new Hashtable();
		mazeName.setText(maze.getName());
		changeMazeNameListener.setMaze(maze);
		
		//Point biggestPoint = setRoomFloorPlanLocations(maze);
		
		Point biggestPoint = new Point(0, 0);
		
		for (int roomIndex = 0; roomIndex < maze.getRooms().length; roomIndex++) {
			Point currentRoomLocation = maze.getRooms()[roomIndex].getFloorPlanLocation();
			
			if (currentRoomLocation.x  > biggestPoint.x) {
				biggestPoint.setLocation(currentRoomLocation.x, biggestPoint.y);
			}
			
			if (currentRoomLocation.y  > biggestPoint.y) {
				biggestPoint.setLocation(biggestPoint.x, currentRoomLocation.y);
			}		
		}
		
		roomsContainer.setLayout(new GridLayout(biggestPoint.y + 1, biggestPoint.x + 1, 6, 6));
		roomsContainer.removeAll();
		
		for (int floorPlanY = 0; floorPlanY <= biggestPoint.y; floorPlanY++) {
			for (int floorPlanX = 0; floorPlanX <= biggestPoint.x; floorPlanX++) {			
				Room roomAtCurrentLocation = maze.getRoomAtFloorPlanLocation(new Point(floorPlanX, floorPlanY));
				
				if (roomAtCurrentLocation != null) {
					FloorPlanRoom currentFloorPlanRoom = new FloorPlanRoom(this, maze, roomAtCurrentLocation);
					floorPlanRoomsOfRealRooms.put(roomAtCurrentLocation, currentFloorPlanRoom);
					roomsContainer.add(currentFloorPlanRoom);					
				} else {
					(roomsContainer.add(new Panel())).setBackground(Color.white);
				}
			}
		}
		
		Room startingRoom = maze.getRooms()[maze.getQuester().getStartingRoom()];
		setSelectedRoom(startingRoom, (FloorPlanRoom)floorPlanRoomsOfRealRooms.get(startingRoom));
		
		validate();
	}
	
	public void setSelectedRoom(Room room, FloorPlanRoom floorPlanRoom) {
		mazeRoomControls.setRoom(room, floorPlanRoom);
	}
	
	class ChangeMazeNameTextListener implements TextListener, java.io.Serializable {
		private Maze maze;
		
		public void setMaze(Maze maze) {
			this.maze = maze;
		}
			
		public void textValueChanged(TextEvent evt) {
			if (maze != null) {
				maze.setName(((TextComponent)evt.getSource()).getText());
				maze.setChanged();
			}
		}
	}
	
	class RoomsContainer extends Container {
		public void paint(Graphics g) {
			setBackground(Color.blue);
			
			for (int roomIndex = 0; roomIndex < maze.getRooms().length; roomIndex++) {
				Room currentRoom = maze.getRooms()[roomIndex];
				FloorPlanRoom currentFloorPlanRoom = (FloorPlanRoom)floorPlanRoomsOfRealRooms.get(currentRoom);
				
				for (int roomItemIndex = 0; roomItemIndex < currentRoom.getItemsInRoom().size(); roomItemIndex++) {
					RoomItem currentRoomItem = (RoomItem)currentRoom.getItemsInRoom().elementAt(roomItemIndex);
					
					if (currentRoomItem instanceof Door) {
						if (currentRoomItem instanceof LockedDoor) {
							g.setColor(Color.red);
						} else {
							g.setColor(Color.black);
						}
						
						FloorPlanRoom destinationFloorPlanRoom = (FloorPlanRoom)floorPlanRoomsOfRealRooms.get(
							maze.getRooms()[(((Door)currentRoomItem).getDestinationRoom())]);
						
						g.drawLine(currentFloorPlanRoom.getLocation().x + currentFloorPlanRoom.getSize().width / 2,
								   currentFloorPlanRoom.getLocation().y + currentFloorPlanRoom.getSize().height / 2,
								   destinationFloorPlanRoom.getLocation().x + destinationFloorPlanRoom.getSize().width / 2,
								   destinationFloorPlanRoom.getLocation().y + destinationFloorPlanRoom.getSize().height / 2);
					}
				}
			}
		}	
	}
}
