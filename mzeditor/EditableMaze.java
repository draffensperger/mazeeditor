package mzeditor;

import mazegame.*;
import java.awt.*;

public class EditableMaze extends Maze {	
	public EditableMaze(int mazeWidth, int mazeHeight, Color startingRoomBgColor) {
		this(new Dimension(mazeWidth, mazeHeight), startingRoomBgColor);
	}
	
	public EditableMaze(Dimension mazeSize, Color startingRoomBgColor) {
		setQuester(new Player(this));
		
		Room[] rooms = new Room[mazeSize.width * mazeSize.height];
		int roomIndex = 0; 
		
		for (int floorPlanX = 0; floorPlanX < mazeSize.width; floorPlanX++) {
			for (int floorPlanY = 0; floorPlanY < mazeSize.height; floorPlanY++) {				
				rooms[roomIndex] = new Room(MazeEditor.getProperties().getProperty("DefaultRoomImage"), startingRoomBgColor);
				rooms[roomIndex].setFloorPlanLocation(new Point(floorPlanX, floorPlanY));
				rooms[roomIndex].setMaze(this);
				roomIndex++;
			}
		}
	
		setRooms(rooms);
		
		setName(MazeEditor.getProperties().getProperty("DefaultMazeName"));
	}
}
