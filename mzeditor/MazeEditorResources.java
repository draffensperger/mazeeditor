package mzeditor;

import java.util.*;
import java.awt.*;

public class MazeEditorResources extends ListResourceBundle {	
	public Object[][] getContents() {
		return contents;
	}
	
	private static final Object[][] contents = {
		{"MazeEditorFrame.Title", "Maze Editor"},
		{"MazeEditorFrame.Open.Title", "Select a Maze to Open"},
		{"MazeEditorFrame.SaveAs.Title", "Save Maze As"},
		{"MazeEditorFrame.NewMazeFileName", "untitled.maze"},
		{"FloorPlanDialog.Title", "Floor Plan"}
	};
}
