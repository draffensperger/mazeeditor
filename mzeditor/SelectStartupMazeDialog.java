package mzeditor;

import java.awt.event.*;
import java.awt.*;
import draff.awt.event.*;
import draff.io.*;
import java.io.*;

public class SelectStartupMazeDialog extends Dialog {
	private Frame frameParent;
	private TextField fileName;
	
	public SelectStartupMazeDialog(Frame parent, String currentStartupFileName, String defaultFileName) {
		super (parent);
		this.frameParent = parent;
		setTitle("Set Startup Maze");
		setModal(true);
		addWindowListener(new WindowCloseWindowListener());
		
		add(new Label("Set the maze that will automatically open when Maze Editor loads:"), "North");
		
		Panel centerPanel = new Panel();
		centerPanel.setLayout(new GridLayout(2, 1));

		Panel row1 = new Panel();
		row1.add(new Label("Current starup file:"));
		Label currentStartup = new Label(currentStartupFileName);
		row1.add(currentStartup);
		centerPanel.add(row1);
		
		Panel row2 = new Panel();
		
		row2.add(new Label("New startup file: "));
		fileName = new TextField(defaultFileName);
		row2.add(fileName);
		((Button)(row2.add(new Button("Browse")))).addActionListener(new BrowseActionListener());				
		centerPanel.add(row2);
		
		add(centerPanel, "Center");
		
		Panel southPanel = new Panel();
		
		Button close = new Button("Close");
		close.addActionListener(new CloseWindowActionListener(this));
		southPanel.add(close);
		add(southPanel, "South");
		
		pack();
	}
	
	class BrowseActionListener implements ActionListener {
		public void actionPerformed(ActionEvent evt) {
			FileDialog browseDialog = new FileDialog(frameParent, "Select a Startup Maze", FileDialog.LOAD);
			browseDialog.pack();
			browseDialog.setVisible(true);
			
			browseDialog.setDirectory((new File("")).getAbsolutePath());
			String initialDirectory = browseDialog.getDirectory();
			
			if (browseDialog.getFile() != null && !browseDialog.getFile().equals("")) {
				fileName.setText(RelativeDirectoryParser.getRelativeDirectory(initialDirectory,
					browseDialog.getDirectory()) + browseDialog.getFile());
			}
		}
	}
	
	public String getFileName() {
		return fileName.getText();
	}
}
