package mzeditor;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

public class AddRoomItemDialog extends Dialog {
	private static final String ROOM_ITEM_FILE = "room_items.txt";
	
	private Choice classNameChoice;
	private TextField constructorParams;
	
	public AddRoomItemDialog(Frame parent) {
		super (parent);
		setTitle("Adding a Room Item");
			
		setLayout(new FlowLayout());
		
		classNameChoice = new Choice();
		
		add(classNameChoice);
		add(new Button("Add new RoomItem class"));
		add(new Button("Remove RoomItem class"));
		
		Panel constructorCallAndLabel = new Panel();
		constructorCallAndLabel.add(new Label("Constructor call: new [class name] ("));
		constructorParams = new TextField();
		constructorCallAndLabel.add(constructorParams);
		constructorCallAndLabel.add(new Label(")"));
		add(constructorCallAndLabel);
		
		add(new Button("Add RoomItem"));
		add(new Button("Cancel"));
	}
	
	/*class BrowseActionListener implements ActionListener {
	}
	
	class CancelActionListener implements ActionListener {
	}
	
	class OKActionListener implements ActionListener {
	}
	
	class SelectedRoomItemChangeListener implements ItemListener {
	}*/
}
