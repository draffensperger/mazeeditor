package mzeditor;

import java.util.*;
import java.io.*;

public class MazeEditor {
	private static final String MAZE_EDITOR_PROPERTIES_FILE = "maze_editor.properties";
	
	private static ResourceBundle resources = ResourceBundle.getBundle("mzeditor.MazeEditorResources");
	private static Properties properties;
	
	static {
		properties = new Properties();
		
		try {
			properties.load(new FileInputStream(MAZE_EDITOR_PROPERTIES_FILE));
		} catch (IOException err) {
			err.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		try {
			//System.setOut(new PrintStream(new FileOutputStream("out.txt")));
			System.setErr(new PrintStream(new FileOutputStream("err.txt")));
			System.getProperties().list(new PrintStream(new FileOutputStream("sysprops.txt")));
		} catch (IOException err) {
			err.printStackTrace();
		}
		
		new MazeEditorFrame();
	}
	
	public static void endProgram() {
		try {
			properties.save(new FileOutputStream(MAZE_EDITOR_PROPERTIES_FILE), "Maze Editor Properties");
		} catch (IOException err) {
			err.printStackTrace();
		}
	}
	
	public static ResourceBundle getResources() {
		return resources;
	}
	
	public static Properties getProperties() {
		return properties;
	}
}
