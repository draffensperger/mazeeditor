package mzeditor;

import java.awt.*;
import java.awt.event.*;
import mazegame.*;
import mazecomponents.*;
import draff.awt.event.*;
import java.io.*;
import draff.awt.*;

public class FloorPlanRoom extends Panel {
	private Room room;
	private FloorPlanContainer floorPlanContainerParent;
	private FloorPlanRoomItem selectedFloorPlanRoomItem;
	private Maze maze;
		
	public FloorPlanRoom(FloorPlanContainer floorPlanContainerParent, Maze maze, Room room) {
		this.floorPlanContainerParent = floorPlanContainerParent;
		this.room = room;
		this.maze = maze;
		addMouseListener(new SelectRoomMouseAdapter());
		updateComponents();
	}

	public void updateComponents() {
		removeAll();
		
		setBackground(room.getProgramBackground());
		
		int roomItemSpaces = Math.max(room.getItemsInRoom().size(),
									  Integer.parseInt(MazeEditor.getProperties().getProperty("MinRoomItemSpaces")));
		int squareRootOfRoomItems = (int)(Math.sqrt(roomItemSpaces));
				
		setLayout(new GridLayout(squareRootOfRoomItems, (int)((double)roomItemSpaces / (double)squareRootOfRoomItems), 1, 1));
							
		for (int roomItemIndex = 0; roomItemIndex < room.getItemsInRoom().size(); roomItemIndex++) {
			RoomItem item = (RoomItem)room.getItemsInRoom().elementAt(roomItemIndex);
			item.loadImage();
			
			(add(new FloorPlanRoomItem(item))).addMouseListener(new SelectRoomItemMouseListener());
		}
		
		for (int remainingSpacesIndex = 0; remainingSpacesIndex < roomItemSpaces - room.getItemsInRoom().size(); remainingSpacesIndex++) {
			Panel spaceFiller = new Panel();
			spaceFiller.setBackground(room.getProgramBackground());
			spaceFiller.addMouseListener(new SelectRoomMouseAdapter());
			add(spaceFiller);
		}
		
		validate();
		repaint();
	}
	
	public void removeSelectedRoomItem() {
		room.removeRoomItem(selectedFloorPlanRoomItem.getRoomItem());
		maze.setChanged();
		remove(selectedFloorPlanRoomItem);
		validate();
		selectedFloorPlanRoomItem = null;
	}
	
	class SelectRoomActionListener implements ActionListener {
		public void actionPerformed(ActionEvent evt) {
			floorPlanContainerParent.setSelectedRoom(FloorPlanRoom.this.room, FloorPlanRoom.this);
		}
	}	
	
	class SelectRoomMouseAdapter extends MouseAdapter {			
		public void mouseReleased(MouseEvent evt) {
			floorPlanContainerParent.setSelectedRoom(FloorPlanRoom.this.room, FloorPlanRoom.this);
			
			if (evt.getClickCount() > 1) {
				showViewFrame();
			}
		}
	}
	
	class SelectRoomItemMouseListener extends MouseAdapter {
		public void mouseReleased(MouseEvent evt) {
			if (evt.getSource() instanceof FloorPlanRoomItem) {
				FloorPlanRoom.this.selectedFloorPlanRoomItem = (FloorPlanRoomItem)evt.getSource();
			}
		}
	}
	
	public Dimension getPreferredSize() {
		return getMinimumSize();
	}
	
	public Dimension getMinimumSize() {
		return new Dimension(60, 60);
	}
	
	public void showViewFrame() {
		Frame roomViewingFrame = new Frame("Viewing Room at (" + room.getFloorPlanLocation().x + 
											   "," + room.getFloorPlanLocation().y + ")");
			roomViewingFrame.addWindowListener(new WindowCloseWindowListener());
			roomViewingFrame.setBackground(room.getProgramBackground());
			
			Panel roomComponentsContainter = new Panel();							 
			roomViewingFrame.add(roomComponentsContainter, "Center");
								
			for (int roomItemIndex = 0; roomItemIndex < room.getItemsInRoom().size(); roomItemIndex++) {
				RoomItem currentItem = (RoomItem)room.getItemsInRoom().elementAt(roomItemIndex);
				currentItem.loadImage();
				currentItem.setSize(currentItem.getPreferredSize());
								
				roomViewingFrame.add(currentItem);
			}
			
			if (new File(room.getBackgroundImageFileName()).exists()) {			
				ImageCanvas background = new ImageCanvas(room.getBackgroundImageFileName());
				background.loadImage();			
					
				background.setSize(background.getPreferredSize());
				background.setLocation(0, 0);
			
				roomViewingFrame.setSize(background.getSize());
				roomViewingFrame.add(background);			
			} else {				
				Label missingImageMessage = new Label("Image: " + room.getBackgroundImageFileName() + " cannot be loaded.");
				roomViewingFrame.add(missingImageMessage, "North");
			}
			
			roomViewingFrame.pack();
			roomViewingFrame.setResizable(false);
			roomViewingFrame.setVisible(true);
	}
}
