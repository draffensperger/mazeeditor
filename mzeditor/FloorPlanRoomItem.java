package mzeditor;

import draff.awt.*;
import mazegame.*;

public class FloorPlanRoomItem extends ImageCanvas {
	private RoomItem roomItem;
	
	public FloorPlanRoomItem(RoomItem roomItem) {
		this.roomItem = roomItem;
		
		setImage(roomItem.getImage());
	}
	
	public RoomItem getRoomItem() {
		return roomItem;
	}
}
