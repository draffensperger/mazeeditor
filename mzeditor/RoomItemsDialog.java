package mzeditor;

import java.awt.*;
import java.awt.event.*;
import draff.awt.event.*;

public class RoomItemsDialog extends Dialog {
	private Panel centerPanel;
	
	public RoomItemsDialog(Frame parent) {
		super(parent);
		
		setTitle("Room Items");
		
		setModal(false);
		
		//Panel westPanel = new Panel();				
		//add(westPanel, "West");	
		
		centerPanel = new Panel();
		add(centerPanel, "Center");
		
		Panel southPanel = new Panel();
		
		//((Button)southPanel.add(new Button("Cancel"))).addActionListener(new OKActionListener());
		//((Button)southPanel.add(new Button("OK"))).addActionListener(new CancelActionListener());
		
		add(southPanel, "South");
	}
	
	public abstract class OKActionListener implements ActionListener {
	}
	
	public abstract class CancelActionListener implements ActionListener {
	}
}
