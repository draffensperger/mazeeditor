package mzeditor;

import java.awt.*;
import java.awt.event.*;
import draff.awt.event.*;
import draff.awt.*;
import draff.io.*;
import mazegame.*;
import java.io.*;
import java.util.*;

public class MazeEditorFrame extends Frame {
	private Maze openMaze;	
	private String openMazeFileName;
	private FloorPlanContainer floorPlanContainer;
	private Vector recentFilesVector;
	
	public MazeEditorFrame() {			
		setUpRecentFilesVector();
		setMenuBar(constructMenuBar());
		
		addWindowListener(new EndProgramWindowListener());
	
		floorPlanContainer = new FloorPlanContainer();
		add(floorPlanContainer);

		if ((new File(MazeEditor.getProperties().getProperty("StartupFileName"))).exists()) {
			openMazeFile(MazeEditor.getProperties().getProperty("StartupFileName"), true);
		} else {
			openMazeFile(null, false);
		}
		
		pack();
		
		//setIconImage(Toolkit.getDefaultToolkit().getImage("images/mazegameicon.jpg"));
		
		setVisible(true);
	}
	
	private MenuBar constructMenuBar() {
		MenuBar floorPlanWindowMenuBar = new MenuBar();
		
		Menu file = new Menu("File");
		
		MenuItem newMaze = new MenuItem("New");
		newMaze.setShortcut(new MenuShortcut(KeyEvent.VK_N));
		newMaze.addActionListener(new NewActionListener());
		file.add(newMaze);
		
		MenuItem open = new MenuItem("Open...");
		open.setShortcut(new MenuShortcut(KeyEvent.VK_O));
		open.addActionListener(new OpenActionListener());
		file.add(open);
		
		MenuItem save = new MenuItem("Save");
		save.setShortcut(new MenuShortcut(KeyEvent.VK_S));
		save.addActionListener(new SaveActionListener());
		file.add(save);
		
		MenuItem saveAs = new MenuItem("Save As...");
		saveAs.addActionListener(new SaveAsActionListener());
		file.add(saveAs);
		
		file.addSeparator();
		
		MenuItem setStartup = new MenuItem("Set Startup Maze...");
		setStartup.addActionListener(new SetStartupActionListener());
		file.add(setStartup);
		
		file.addSeparator();
		
		boolean areAnyRecentFilesInMenu = false;
		
		for (int recentFileIndex = 0; recentFileIndex < recentFilesVector.size(); recentFileIndex++) {			
			String recentFileName = (String)recentFilesVector.elementAt(recentFileIndex);
			
			MenuItem recentFileMenuItem = new MenuItem(recentFileName);
			recentFileMenuItem.addActionListener(new RecentFileActionListener());
			//recentFileMenuItem.setShortcut(new MenuShortcut(KeyEvent.VK_1 + recentFileIndex));
			file.add(recentFileMenuItem);
		}
		
		if (recentFilesVector.size() > 0) {	
			file.addSeparator();
		}
		
		MenuItem exit = new MenuItem("Exit");
		exit.addActionListener(new EndProgramActionListener());
		file.add(exit);
		
		floorPlanWindowMenuBar.add(file);
		
		Menu edit = new Menu("Edit");
		((MenuItem)edit.add(new MenuItem("Play maze"))).addActionListener(new PlayMazeActionListener());
		floorPlanWindowMenuBar.add(edit);
		
		Menu help = new Menu("Help");
		help.add(new MenuItem("How to Use Maze Editor..."));
		floorPlanWindowMenuBar.add(help);
		
		return floorPlanWindowMenuBar;
	}
	
	public Maze getOpenMaze() {
		return openMaze;
	}
	
	public void setOpenMaze(Maze openMaze) {
		this.openMaze = openMaze;
		
		openMaze.setChangeObserver(new MazeChangeObserver());
		openMaze.clearChanged();
		
		floorPlanContainer.setMaze(openMaze);
	}
	
	public String getOpenMazeFileName() {
		String openMazeFileNameResult = openMazeFileName;
		
		if (openMazeFileNameResult == null) {
			openMazeFileNameResult = MazeEditor.getProperties().getProperty("NewFileName");
		}
		
		return openMazeFileNameResult;
	}
	
	public void setOpenMazeFileName(String openMazeFileName) {
		this.openMazeFileName = openMazeFileName;
	}
		
	class NewActionListener implements ActionListener {
		public void actionPerformed(ActionEvent evt) {
			if (saveIfConfirmedAndReturnResult() != ConfirmDialog.CANCELED) {	
				String[] inputNames = {"Width", "Height", "Background color:  Red", "Green", "Blue"};					
				String[] defaultValues = {MazeEditor.getProperties().getProperty("DefaultMazeWidth"),
										  MazeEditor.getProperties().getProperty("DefaultMazeHeight"),
										  MazeEditor.getProperties().getProperty("DefaultRoomBackgroundColor.Red"),
										  MazeEditor.getProperties().getProperty("DefaultRoomBackgroundColor.Green"),
										  MazeEditor.getProperties().getProperty("DefaultRoomBackgroundColor.Blue")};
			
				InputDialog mazeSizeDialog = new InputDialog(MazeEditorFrame.this, "Enter Maze Variables", inputNames, defaultValues);
				mazeSizeDialog.setVisible(true);
			
				try {
					// The inputs will be null if the dialog was canceled
					if (mazeSizeDialog.getInput(0) != null && mazeSizeDialog.getInput(1) != null) {					
							setOpenMaze(new EditableMaze(Integer.parseInt(mazeSizeDialog.getInput(0)), Integer.parseInt(mazeSizeDialog.getInput(1)),
														 new Color(Integer.parseInt(mazeSizeDialog.getInput(2)),
																   Integer.parseInt(mazeSizeDialog.getInput(3)),
																   Integer.parseInt(mazeSizeDialog.getInput(4)))));
							setOpenMazeFileName(MazeEditor.getProperties().getProperty("NewFileName"));
							updateTitle();
					} else {
						return;
					}
				} catch (NumberFormatException badInput) {
					openMazeFile(null, false);
				}
			}
		}
	}
	
	class OpenActionListener implements ActionListener {
		public void actionPerformed(ActionEvent evt) {
			if (saveIfConfirmedAndReturnResult() != ConfirmDialog.CANCELED) {
				FileDialog openDialog = new FileDialog(MazeEditorFrame.this, MazeEditor.getResources().getString("MazeEditorFrame.Open.Title"),
													   FileDialog.LOAD);
				openDialog.setVisible(true);
				String fileName = openDialog.getFile();
			
				if (fileName != null) {
					openMazeFile(fileName, false);
				}
			}
		}
	}
	
	class SaveActionListener implements ActionListener {
		public void actionPerformed(ActionEvent evt) {
			if (new File(getOpenMazeFileName()).exists()) {					
				saveMazeFile(getOpenMazeFileName());
				getOpenMaze().clearChanged();
			} else {
				new SaveAsActionListener().actionPerformed(null);
			}
		}
	}
	
	class SaveAsActionListener implements ActionListener {
		public void actionPerformed(ActionEvent evt) {
			FileDialog openDialog = new FileDialog(MazeEditorFrame.this, MazeEditor.getResources().getString("MazeEditorFrame.SaveAs.Title"),
												   FileDialog.SAVE);
			openDialog.setFile(getOpenMazeFileName());
			openDialog.setVisible(true);
			String fileName = openDialog.getFile();
			
			setOpenMazeFileName(fileName);
			saveMazeFile(fileName);
			updateTitle();
			addRecentFile(fileName);
			getOpenMaze().clearChanged();
		}
	}
	
	class SetStartupActionListener implements ActionListener {
		public void actionPerformed(ActionEvent evt) {
			SelectStartupMazeDialog selectDialog = new SelectStartupMazeDialog(MazeEditorFrame.this, 
																			   MazeEditor.getProperties().getProperty("StartupFileName"), 
																			   getOpenMazeFileName());			
			selectDialog.setVisible(true);
			MazeEditor.getProperties().put("StartupFileName", selectDialog.getFileName());
		}
	}
	
	class RecentFileActionListener implements ActionListener {
		public void actionPerformed(ActionEvent evt) {
			if (saveIfConfirmedAndReturnResult() != ConfirmDialog.CANCELED) {
				if (evt.getActionCommand() != null) {
					openMazeFile(evt.getActionCommand(), false);
				}
			}
		}
	}
	
	class PlayMazeActionListener implements ActionListener {
		public void actionPerformed(ActionEvent evt) {
			try {
				Runtime.getRuntime().exec(MazeEditor.getProperties().getProperty("PlayMazePath") + " " 
										  + getOpenMazeFileName());
			} catch (IOException exception) {				
				new MessageDialog(MazeEditorFrame.this, "Problem Playing Maze", exception.getMessage());
			}
		}
	}

	private void openMazeFile(String fileName, boolean openingStartup) {
		try {		
			if (fileName == null) {						
				fileName = MazeEditor.getResources().getString("MazeEditorFrame.NewMazeFileName"); 			
				setOpenMaze(new EditableMaze(Integer.parseInt(MazeEditor.getProperties().getProperty("DefaultMazeWidth")), 
											 Integer.parseInt(MazeEditor.getProperties().getProperty("DefaultMazeHeight")),
											 new Color(Integer.parseInt(MazeEditor.getProperties().getProperty("DefaultRoomBackgroundColor.Red")),
													   Integer.parseInt(MazeEditor.getProperties().getProperty("DefaultRoomBackgroundColor.Green")),
													   Integer.parseInt(MazeEditor.getProperties().getProperty("DefaultRoomBackgroundColor.Blue")))));
			} else {				
				setOpenMaze((Maze)(new ObjectInputStream(new FileInputStream(fileName))).readObject());				
				addRecentFile(fileName);
			}
			
			setOpenMazeFileName(fileName);
			updateTitle();
		} catch (FileNotFoundException fileNotFoundException) {
			fileNotFoundException.printStackTrace(System.err);
			if (openingStartup) {
				new MessageDialog(this, "Startup File Not Found", "The startup file: " + fileName + ", was not found. \n" +
																  "A new maze has been created instead.");
				openMazeFile(null, false);
			} else {
				new MessageDialog(this, "Maze File Not found", "The file: " + fileName + ", was not found.");
			}
		} catch (Exception exception) {
			exception.printStackTrace(System.err);
			if (openingStartup) {
				new MessageDialog(this, "Problem Opening Startup Maze", "There was an exception: " + exception.getMessage() +
																		"\n while opening the startup file: " + fileName + 
																		"\n A new maze has been created instead.");
				openMazeFile(null, false);
			} else {
				new MessageDialog(this, "Problem Opening Maze", exception.getMessage());
			}
		}
	}
	
	private void saveMazeFile(String fileName) {
		try {
			(new ObjectOutputStream(new FileOutputStream(fileName))).writeObject(getOpenMaze());
		} catch (IOException err) {
			err.printStackTrace();
		}		
	}
	
	private void addRecentFile(String fileName) {
		if (recentFilesVector.contains(fileName)) {		
			recentFilesVector.removeElement(fileName);
		}
		
		recentFilesVector.insertElementAt(fileName, 0);
		
		setMenuBar(constructMenuBar());
	}
	
	private void setUpRecentFilesProperties() {
		for (int recentFileIndex = 0; recentFileIndex < recentFilesVector.size(); recentFileIndex++) {			
			MazeEditor.getProperties().put("RecentFile" + recentFileIndex, (String)recentFilesVector.elementAt(recentFileIndex));
		}		
	}
	
	private void setUpRecentFilesVector() {
		recentFilesVector = new Vector();
		
		for (int recentFileIndex = 0; recentFileIndex < Integer.parseInt(MazeEditor.getProperties().getProperty("NumRecentFiles")); 
			 recentFileIndex++) {
			
			String recentFileName = MazeEditor.getProperties().getProperty("RecentFile" + recentFileIndex);
			
			if (!recentFileName.equals("")) {
				recentFilesVector.addElement(recentFileName);
			}
		}
	}
	
	class EndProgramWindowListener extends WindowAdapter {
		public void windowClosing(WindowEvent evt) {
			new EndProgramActionListener().actionPerformed(null);
		}
	}
	
	class EndProgramActionListener implements ActionListener {
		public void actionPerformed(ActionEvent evt) {
			if (saveIfConfirmedAndReturnResult() != ConfirmDialog.CANCELED) {
				endProgram();
			}
		}
	}
	
	private void endProgram() {
		setUpRecentFilesProperties();			
		MazeEditor.endProgram();
		System.exit(0);
	}
	
	private int saveIfConfirmedAndReturnResult() {
		if (getOpenMaze().hasChanged()) {		
			int confirmResult = (new ConfirmDialog(this, "Save Current Maze?", "Do you want to save the current maze?")).getResult();
		
			if (confirmResult == ConfirmDialog.YES) {
				new SaveActionListener().actionPerformed(null);
			}

			return confirmResult;
		} else {
			getOpenMaze().clearChanged();
			return ConfirmDialog.NO;
		}
	}
	
	class MazeChangeObserver implements Observer, java.io.Serializable {
		public void update(Observable maze, Object hasChanged) {
			updateTitle();
		}
	}
	
	private void updateTitle() {
		String starIfChanged = "";
		
		if (getOpenMaze().hasChanged()) {
			starIfChanged = "*";
		}
		
		setTitle(MazeEditor.getResources().getString("MazeEditorFrame.Title") + " - " + getOpenMazeFileName() + starIfChanged);
	}
}
