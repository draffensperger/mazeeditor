package mazecomponents;

import java.awt.event.*;
import java.awt.*;
import mazegame.*;

/**
 * A door is a RoomItem that leads to another room.
 *
 * @author David Raffensperger
 * @see RoomItem
 */
public class Door extends RoomItem {
	/** The index of the room in the array returned by Maze.getRooms() to which this leads. */
	private int destinationRoom;
	
	public Door(String normalImageFile, Point normalLocation, String mouseOverImageFile, 
				Point mouseOverImageLocation, int destinationRoom) {
		super (normalImageFile, normalLocation, mouseOverImageFile, mouseOverImageLocation);
		this.destinationRoom = destinationRoom;
	}																							 
	
	/**
	 * Sends the quester to the destination room and makes him face in the destination direction.
	 * @param actor What inventory did the action to this.
	 * @see MazeItem#actedUpon(InventoryItem)
	 */
	public void mousePressed(MouseEvent evt) {
		getMaze().prepareForChange();
		getMaze().getQuester().setRoomLocatedIn(destinationRoom);
		getMaze().update();
	}
	
	public int getDestinationRoom() {
		return destinationRoom;
	}
}