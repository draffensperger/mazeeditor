package mazecomponents;

import mazegame.*;

public class DefaultInventoryItem extends InventoryItem {
	public DefaultInventoryItem(String imageFileName) {
		super (imageFileName, "DefaultInventoryItem");
	}
}
