package mazecomponents;

import java.util.*;
import mazegame.*;
import java.awt.*;
import java.awt.event.*;

public class EffectMapRoomItem extends RoomItem {
	private Vector inventoryEffects;
	private Player player;
	
	public EffectMapRoomItem(String normalImageFile, Point normalLocation, String mouseOverImageFile, 
							 Point mouseOverImageLocation) {
		super (normalImageFile, normalLocation, mouseOverImageFile, mouseOverImageLocation);
		inventoryEffects = new Vector();
	}
	
	public void mousePressed(MouseEvent evt) {
		for (int effectIndex = 0; effectIndex < inventoryEffects.size(); effectIndex++) {
			InventoryEffect currentEffect = (InventoryEffect)inventoryEffects.elementAt(effectIndex);
			
			if (player.has(currentEffect.getInventory())) {
				currentEffect.performEffect();
				break;
			}
		}
	}
	
	public void setPlayer(Player player) {
		this.player = player;
	}
	
	public void addInventoryEffect(InventoryEffect inventoryEffect) {
		inventoryEffects.addElement(inventoryEffect);
	}
	
	public void addInventoryEffect(InventoryEffect inventoryEffect, int position) {
		inventoryEffects.insertElementAt(inventoryEffect, position);
	}
	
	public void removeInventoryEffect(InventoryEffect inventoryEffect) {
		inventoryEffects.removeElement(inventoryEffect);
	}
}
