package mazecomponents;

import java.awt.event.*;
import mazegame.*;
import java.awt.Point;

public class DeathTrap extends RoomItem {
	public DeathTrap(String imageFileName, Point location) {
		super (imageFileName, location, imageFileName, location);
	}
	
	public void mousePressed(MouseEvent evt) {
		getMaze().getQuester().lost();
	}
}
