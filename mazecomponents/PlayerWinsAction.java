package mazecomponents;

import mazegame.*;

public class PlayerWinsAction extends Action {
	private Player player;
	
	public PlayerWinsAction(Player player) {
		this.player = player;
	}
	
	public void performAction() {
		player.won();
	}
}
