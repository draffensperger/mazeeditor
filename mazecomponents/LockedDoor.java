package mazecomponents;

import java.awt.event.*;
import mazegame.*;
import java.awt.Point;
// Bad item locations
// Bad room sizes
// Bad imge qualities
// Bad update when item gotten
// Inv display must start size

public class LockedDoor extends Door {
	private boolean isLocked = true;
	private String unlockedImageFile;
	private String lockedImageFile;	
	private String keyToUnlock;
	
	public LockedDoor(String lockedImageFile, String unlockedImageFile, Point normalLocation, 
					  String mouseOverImageFile, Point mouseOverLocation, int destinationRoom, 
					  boolean isLocked, String keyToUnlock) {
		super(isLocked ? lockedImageFile : unlockedImageFile, normalLocation, 
			  mouseOverImageFile, mouseOverLocation, destinationRoom);
		
		this.lockedImageFile = lockedImageFile;
		this.unlockedImageFile = unlockedImageFile;
		this.keyToUnlock = keyToUnlock;
		setLocked(true);
	}
	
	public void mousePressed(MouseEvent evt) {
		if (isLocked && getMaze().getQuester().has(keyToUnlock)) {
			setLocked(false);
		} else if (!isLocked) {
			super.mousePressed(evt);
		}
	}
	
	public void mouseEntered(MouseEvent evt) {
		if (!isLocked) {
			super.mouseEntered(evt);
		}
	}
	
	public void mouseExited(MouseEvent evt) {
		if (!isLocked) {
			super.mouseExited(evt);
		}
	}
	
	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;

		setNormalImageFileName(isLocked ? lockedImageFile : unlockedImageFile);
		mouseEntered(null);
		
		repaint(MazeContainer.REPAINT_DELAY);
	}
}
