package mazecomponents;

import java.awt.event.*;
import mazegame.*;
import java.awt.Point;

public class VictoryTreasure extends RoomItem {
	public VictoryTreasure(String imageFileName, Point location) {
		super (imageFileName, location);
	}
	
	public VictoryTreasure(String imageFileName, int xLocation, int yLocation) {
		super (imageFileName, new Point(xLocation, yLocation));
	}
	
	public void mousePressed(MouseEvent evt) {
		getMaze().getQuester().won();
	}
}
