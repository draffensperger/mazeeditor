package mazecomponents;

import mazegame.*;

public class PlayerLosesAction extends Action {
	private Player player;
	
	public PlayerLosesAction(Player possibleLoser) {
		player = possibleLoser;
	}
	
	public void performAction() {
		player.lost();
	}
}
