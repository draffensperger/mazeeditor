package mazegenerator;

import java.io.*;
import mazegame.*;
import draff.lang.reflect.*;
import java.util.Vector;

/*
Example string:
"Mission II

new mazecomponents.SomeInventoryItem
new mazecomponents.AnotherInventoryItem

new mazegame.Room()
	new mazecomponents.Item()
	new mazecomponents.Item()
	new mazecomponents.Item()
new mazegame.Room(2)
	new mazecomponents.Item()"
*/
public class LoadedFromStringMaze extends Maze {
	public LoadedFromStringMaze(InputStream source) {
		LineNumberReader lineReader = new LineNumberReader(new InputStreamReader(source));
		String currentLine = null;
		
		try {
			currentLine = lineReader.readLine();			
			setName(currentLine);
		
			// the line after the mission name is blank
			lineReader.readLine();
			
			Player quester = new Player(this);
			Vector rooms = new Vector();
			currentLine = lineReader.readLine();
			boolean firstInventoryItem = true;
			while(currentLine != null) {
				Object currentThing = ExpressionDecoder.constructObject(currentLine);
				
				if (currentThing instanceof Room) {
					Room currentRoom = (Room)currentThing;
					currentRoom.setMaze(this);
				
					currentLine = lineReader.readLine();
					while (currentLine != null && currentLine.startsWith("\t")) {					
						
						RoomItem itemToBeAdded = (RoomItem)ExpressionDecoder.constructObject(currentLine);

						itemToBeAdded.setMaze(this);
						itemToBeAdded.loadImage();
						itemToBeAdded.setSize(itemToBeAdded.getPreferredSize());
						System.out.println(itemToBeAdded);
						currentRoom.addRoomItem(itemToBeAdded);
						
						currentLine = lineReader.readLine();
					}
				
					rooms.addElement(currentRoom);
				} else {
					InventoryItem itemToBeAdded = (InventoryItem)currentThing;
					quester.addInventoryItem(itemToBeAdded);				
					currentLine = lineReader.readLine();
					
					if (firstInventoryItem) {
						firstInventoryItem = false;
					}
				}
			}
			
			setQuester(quester);
			 
			Room[] roomArray = new Room[rooms.size()];
			rooms.copyInto(roomArray);
			setRooms(roomArray);
		} catch (ArrayIndexOutOfBoundsException outOfBounds) {
		} catch (Exception err) {
			System.out.println("Exception while reading line " + lineReader.getLineNumber() + ":");
			System.out.println("Current line reads: " + currentLine);
			err.printStackTrace();
		}
	}
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}