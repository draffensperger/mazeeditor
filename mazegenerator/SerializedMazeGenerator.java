
package mazegenerator;

import mazegame.*;
import java.io.*;
import java.util.*;

public class SerializedMazeGenerator {
	public static void main(String[] args) {
		ObjectOutputStream out = null;
		InputStream in = null;
		
		try {			
			String inputFileName = null;
			String outputFileName = null;
			
			try {
				inputFileName = args[0];
				outputFileName = args[1];
			} catch (ArrayIndexOutOfBoundsException err) {
				System.out.println("Syntax: SerializedMazeGenerator <maze properties file> <destination serialized object file>");
			}
		
			//Properties mazeData = new Properties();			
			//mazeData.load(in);
			in = new FileInputStream(inputFileName);
			
			out = new ObjectOutputStream(new FileOutputStream(outputFileName));
			out.writeObject(new LoadedFromStringMaze(in));
			System.out.println("The maze loaded from " + inputFileName + " has been serialized in " + outputFileName + ".");
		} catch (Exception err) {
			err.printStackTrace();
		}
	
		try {
			in.close();
			out.close();
		} catch (Exception err) {
			err.printStackTrace();
		}
	}
}
