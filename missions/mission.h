#define quote(x) #x
// Change the directory as necessary depending on the location of images
#define img(image) quote(images/image)
#define pt(x, y) new java.awt.Point(x, y)

#define room(image, r, g, b) new mazegame.Room(img(image), new java.awt.Color(r, g, b))

// Room item shorthands
#define treasure(image, x, y) new mazecomponents.VictoryTreasure(img(image), pt(x, y))
#define keyritem(image, x, y, keyimage, keyname) \
	new mazecomponents.KeyRoomItem(img(image), pt(x, y), img(keyimage), quote(keyname))
#define trap(image, x, y) new mazecomponents.DeathTrap(img(image), pt(x, y))
#define door(image, x, y, mouseover, mox, moy, destination) new mazecomponents.Door(img(image), pt(x, y), img(mouseover), \
	pt(mox, moy), destination)
#define lockeddoor(lockedimage, unlockedimage, x, y, mouseover, mox, moy, destination, islocked, keycode) \
	new mazecomponents.LockedDoor(img(lockedimage), img(unlockedimage), pt(x, y), img(mouseover), \
	pt(mox, moy), destination, islocked, quote(keycode))
#define inv(image, name) new mazegame.InventoryItem(img(image), quote(name))
