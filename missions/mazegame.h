#include "mission.h"

// Room shorthands
#define brass room(marble&stars.jpg, 0, 102, 0)
#define sapphire room(granite&clouds.jpg, 128, 128, 128)
#define greenmarble room(wrongdoor.jpg, 102, 51, 0)
#define wood room(treasure.jpg, 102, 0, 102)

// RoomItem shorthands:
#define fdoorsap(destination) door(wnutFclosed.gif, 187, 149, wnutFopen.gif, 187, 135, destination)
#define bdoorsap(destination) door(backarrow.gif, 183, 285, backwards.gif, 183, 285, destination)
#define rdoorsap(destination) door(wnutRclosed.gif, 365, 148, wnutRopen.gif, 365, 150, destination)
#define ldoorsap(destination) door(wnutLclosed.gif, 27, 148, wnutLopen.gif, 27, 148, destination)
#define llockeddoorsap(destination, keycode) lockeddoor(wnutLlocked##keycode##.gif, \
	wnutLclosed.gif, 27, 148, wnutLopen.gif, 27, 148, destination, true, key##keycode)
#define rlockeddoorsap(destination, keycode) lockeddoor(wnutRlocked##keycode##.gif, \
	wnutRclosed.gif, 365, 148, wnutRopen.gif, 365, 148, destination, true, key##keycode)

#define fdoorbrass(destination) door(oakFclosed.gif, 191, 148, oakFopen.gif, 191, 134, destination)
#define bdoorbrass(destination) bdoorsap(destination)
#define rdoorbrass(destination) door(oakRclosed.gif, 365, 148, oakRopen.gif, 365, 148, destination)
#define ldoorbrass(destination) door(oakLclosed.gif, 27, 148, oakLopen.gif, 27, 148, destination)

#define key(keycode) keyritem(keyroomitem##keycode##.gif, 212, 264, keyinventoryitem##keycode##.gif, key##keycode)

#define chest treasure(chest.gif, 175, 241)
#define youlost trap(oops.gif, 132, 107)

// InventoryItem shorthands
#define hand inv(hand.gif, hand)
#define filler inv(inventory.jpg, filler)